To compile this note on Ubuntu, the following packages are needed:

    texlive-base
    texlive-latex-base
    texlive-latex-recommended
    texlive-latex-extra
    texlive-humanities
